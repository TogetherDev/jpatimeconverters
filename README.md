# JPA Time Converters #

#Versions

* **1.1**
* 1.0

## What is it?

This is library that provides time converters, that converts a given java  time object to  a type that is compatible with the relational databases. This converters implements the interface `javax.persistence.AttributeConverter` therefore, it are compatible with the *JPA API*.

It provides support for the following types:

* Duration
* Instant
* LocalDate
* LocalDateTime
* LocalTime
* MonthDay
* OffsetDateTime
* OffsetTime
* Period
* Year
* YearMonth
* ZoneId
* ZoneOffset
* ZonedDateTime

## Using JPA Time Converters

You can use our maven repository, that is easier, or you can download it and build the Jar file and later add to your project.
## Maven Repository

If in your pom.xml file have not the [TogetherDev Maven Repository](https://bitbucket.org/TogetherDev/mavenrepository) then, add this code.

```
#!xml

<repository>
    <id>togetherdev</id>
    <url>https://bitbucket.org/TogetherDev/mavenrepository/raw/master</url>
</repository>

```

And then, add this dependency:

```
#!xml
<dependency>
    <groupId>com.togetherdev</groupId>
    <artifactId>jpa-time-converters</artifactId>
    <version>1.1</version>
</dependency>
```
## Use example:

Adds the annotation `@Convert` (*javax.persistence.Convert*) on the field, that you desire that be converted to persist, and sets the *conveter attribute* with a converter that is compatible with the field type and that best adapts to your domain.

```
#!java

public class Person {

    @Convert(converter = LocalDateConverters.ToDate.class)
    private LocalDate birthDate;

}

```

Later add the converter class, that you used, in your persistence.xml file.

```
#!xml
<class>com.togetherdev.util.time.converter.LocalDateConverters$ToDate</class>
```

## Licensing

**JPA Time Converters** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.