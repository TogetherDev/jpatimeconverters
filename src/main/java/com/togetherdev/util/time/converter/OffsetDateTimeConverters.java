/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.OffsetDateTime;
import static java.time.OffsetDateTime.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain OffsetDateTime}.
 *
 * @author Thomás Sousa Silva
 */
public final class OffsetDateTimeConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private OffsetDateTimeConverters() {
        throw new AssertionError("No OffsetDateTimeConverters instances for you!");
    }

    /**
     * Converter of {@linkplain OffsetDateTime} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.OffsetDateTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<OffsetDateTime, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain OffsetDateTime#toString() toString method} to convert.</p>
         *
         * @param offsetDateTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(OffsetDateTime offsetDateTime) {
            return ((offsetDateTime == null) ? null : offsetDateTime.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain OffsetDateTime#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.OffsetDateTime) convertToDatabaseColumn
         */
        @Override
        public OffsetDateTime convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
