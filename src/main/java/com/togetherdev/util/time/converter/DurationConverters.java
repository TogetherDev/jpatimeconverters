/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.Duration;
import static java.time.Duration.ofDays;
import static java.time.Duration.ofHours;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static java.time.Duration.ofNanos;
import static java.time.Duration.ofSeconds;
import static java.time.Duration.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain Duration}.
 *
 * @author Thomás Sousa Silva
 */
public final class DurationConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private DurationConverters() {
        throw new AssertionError("No DurationConverters instances for you!");
    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToSeconds implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#getSeconds() getSeconds method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.getSeconds());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofSeconds(long) ofHours method} to convert.</p>
         *
         * @param seconds The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long seconds) {
            return ((seconds == null) ? null : ofSeconds(seconds));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToMinutes implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toMinutes() toMinutes method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toMinutes());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofMinutes(long) ofHours method} to convert.</p>
         *
         * @param minutes The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long minutes) {
            return ((minutes == null) ? null : ofMinutes(minutes));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToHours implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toHours() toHours method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toHours());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofHours(long) ofHours method} to convert.</p>
         *
         * @param hours The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long hours) {
            return ((hours == null) ? null : ofHours(hours));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToDays implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toDays() toDays method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toDays());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofDays(long) ofDays method} to convert.</p>
         *
         * @param days The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long days) {
            return ((days == null) ? null : ofDays(days));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToMilis implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toMillis() toMillis method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toMillis());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofMillis(long) ofMillis method} to convert.</p>
         *
         * @param millis The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long millis) {
            return ((millis == null) ? null : ofMillis(millis));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToNanos implements AttributeConverter<Duration, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toNanos() toNanos method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toNanos());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#ofNanos(long) ofNanos method} to convert.</p>
         *
         * @param nanos The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(Long nanos) {
            return ((nanos == null) ? null : ofNanos(nanos));
        }

    }

    /**
     * Converter of {@linkplain Duration} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<Duration, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Duration#toString() toString method} to convert.</p>
         *
         * @param duration The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(Duration duration) {
            return ((duration == null) ? null : duration.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Duration#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Duration) convertToDatabaseColumn
         */
        @Override
        public Duration convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
