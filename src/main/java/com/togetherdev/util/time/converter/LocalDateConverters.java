/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.sql.Date;
import static java.sql.Date.valueOf;
import java.time.LocalDate;
import static java.time.LocalDate.ofEpochDay;
import static java.time.LocalDate.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain LocalDate}.
 *
 * @author Thomás Sousa Silva
 */
public final class LocalDateConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private LocalDateConverters() {
        throw new AssertionError("No LocalDateConverters instances for you!");
    }

    /**
     * Converter of {@linkplain LocalDate} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToEpochDay implements AttributeConverter<LocalDate, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalDate#toEpochDay() toEpochDay method} to convert.</p>
         *
         * @param localDate The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(LocalDate localDate) {
            return ((localDate == null) ? null : localDate.toEpochDay());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalDate#ofEpochDay(long) ofEpochDay method} to convert.</p>
         *
         * @param epochDay The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
         */
        @Override
        public LocalDate convertToEntityAttribute(Long epochDay) {
            return ((epochDay == null) ? null : ofEpochDay(epochDay));
        }

    }

    /**
     * Converter of {@linkplain LocalDate} to {@linkplain Date}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.sql.Date) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToDate implements AttributeConverter<LocalDate, Date> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Date#valueOf(java.time.LocalDate) valueOf method} to convert.</p>
         *
         * @param localDate The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.sql.Date) convertToEntityAttribute
         */
        @Override
        public Date convertToDatabaseColumn(LocalDate localDate) {
            return ((localDate == null) ? null : valueOf(localDate));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Date#toLocalDate() toLocalDate method} to convert.</p>
         *
         * @param date The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
         */
        @Override
        public LocalDate convertToEntityAttribute(Date date) {
            return ((date == null) ? null : date.toLocalDate());
        }

    }

    /**
     * Converter of {@linkplain LocalDate} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<LocalDate, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalDate#toString() toString method} to convert.</p>
         *
         * @param localDate The value that will be converted.
         * @return The conversion result or null if the given value is null.
         */
        @Override
        public String convertToDatabaseColumn(LocalDate localDate) {
            return ((localDate == null) ? null : localDate.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalDate#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDate) convertToDatabaseColumn
         */
        @Override
        public LocalDate convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
