/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.Period;
import static java.time.Period.ofDays;
import static java.time.Period.ofMonths;
import static java.time.Period.ofWeeks;
import static java.time.Period.ofYears;
import static java.time.Period.parse;
import static java.time.temporal.ChronoUnit.WEEKS;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain Period}.
 *
 * @author Thomás Sousa Silva
 */
public final class PeriodConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private PeriodConverters() {
        throw new AssertionError("No PeriodConverters instances for you!");
    }

    /**
     * Converter of {@linkplain Period} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToDays implements AttributeConverter<Period, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Period#getDays() getDays method} to convert.</p>
         *
         * @param period The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(Period period) {
            return ((period == null) ? null : period.getDays());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Period#ofDays(int) ofDays method} to convert.</p>
         *
         * @param days The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
         */
        @Override
        public Period convertToEntityAttribute(Integer days) {
            return ((days == null) ? null : ofDays(days));
        }

    }

    /**
     * Converter of {@linkplain Period} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToWeeks implements AttributeConverter<Period, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Period#get(java.time.temporal.TemporalUnit) get method} to convert, using {@linkplain java.time.ChronoUnit#WEEKS WEEKS}.</p>
         *
         * @param period The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(Period period) {
            return ((period == null) ? null : (int) period.get(WEEKS));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Period#ofWeeks(int) ofWeeks method} to convert.</p>
         *
         * @param weeks The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
         */
        @Override
        public Period convertToEntityAttribute(Integer weeks) {
            return ((weeks == null) ? null : ofWeeks(weeks));
        }

    }

    /**
     * Converter of {@linkplain Period} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToMonths implements AttributeConverter<Period, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Period#getMonths() getMonths method} to convert.</p>
         *
         * @param period The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(Period period) {
            return ((period == null) ? null : period.getMonths());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Period#ofMonths(int) ofMonths method} to convert.</p>
         *
         * @param months The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
         */
        @Override
        public Period convertToEntityAttribute(Integer months) {
            return ((months == null) ? null : ofMonths(months));
        }

    }

    /**
     * Converter of {@linkplain Period} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToYears implements AttributeConverter<Period, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Period#getYears() getYears method} to convert.</p>
         *
         * @param period The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(Period period) {
            return ((period == null) ? null : period.getYears());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Period#ofYears(int) ofYears method} to convert.</p>
         *
         * @param years The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
         */
        @Override
        public Period convertToEntityAttribute(Integer years) {
            return ((years == null) ? null : ofYears(years));
        }

    }

    /**
     * Converter of {@linkplain Period} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<Period, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Period#toString() toString method} to convert.</p>
         *
         * @param period The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(Period period) {
            return ((period == null) ? null : period.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Period#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Period) convertToDatabaseColumn
         */
        @Override
        public Period convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
