/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.sql.Timestamp;
import static java.sql.Timestamp.valueOf;
import java.time.LocalDateTime;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.LocalDateTime.parse;
import static java.time.ZoneOffset.UTC;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain LocalDateTime}.
 *
 * @author Thomás Sousa Silva
 */
public final class LocalDateTimeConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private LocalDateTimeConverters() {
        throw new AssertionError("No LocalDateTimeConverters instances for you!");
    }

    /**
     * Converter of {@linkplain LocalDateTime} to {@linkplain Timestamp}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.sql.Timestamp) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToTimestamp implements AttributeConverter<LocalDateTime, Timestamp> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Timestamp#valueOf(java.time.LocalDateTime) valueOf method} to convert.</p>
         *
         * @param localDateTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.sql.Timestamp) convertToEntityAttribute
         */
        @Override
        public Timestamp convertToDatabaseColumn(LocalDateTime localDateTime) {
            return ((localDateTime == null) ? null : valueOf(localDateTime));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Timestamp#toLocalDateTime() toLocalDateTime method} to convert.</p>
         *
         * @param timestamp The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
         */
        @Override
        public LocalDateTime convertToEntityAttribute(Timestamp timestamp) {
            return ((timestamp == null) ? null : timestamp.toLocalDateTime());
        }

    }

    /**
     * Converter of {@linkplain LocalDateTime} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToEpochSecond implements AttributeConverter<LocalDateTime, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalDateTime#toEpochSecond(java.time.ZoneOffset) toEpochSecond method} to convert, using {@linkplain java.time.ZoneOffset#UTC UTC}.</p>
         *
         * @param localDateTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(LocalDateTime localDateTime) {
            return ((localDateTime == null) ? null : localDateTime.toEpochSecond(UTC));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalDateTime#ofEpochSecond(long, int, java.time.ZoneOffset) ofEpochSecond method} to convert, using zero nano seconds and {@linkplain java.time.ZoneOffset#UTC UTC}.</p>
         *
         * @param epochSecond The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
         */
        @Override
        public LocalDateTime convertToEntityAttribute(Long epochSecond) {
            return ((epochSecond == null) ? null : ofEpochSecond(epochSecond, 0, UTC));
        }

    }

    /**
     * Converter of {@linkplain LocalDateTime} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<LocalDateTime, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalDateTime#toString() toString method} to convert.</p>
         *
         * @param localDateTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(LocalDateTime localDateTime) {
            return ((localDateTime == null) ? null : localDateTime.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalDateTime#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalDateTime) convertToDatabaseColumn
         */
        @Override
        public LocalDateTime convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
