/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.ZonedDateTime;
import static java.time.ZonedDateTime.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain ZonedDateTime}.
 *
 * @author Thomás Sousa Silva
 */
public final class ZonedDateTimeConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private ZonedDateTimeConverters() {
        throw new AssertionError("No ZonedDateTimeConverters instances for you!");
    }

    /**
     * Converter of {@linkplain ZonedDateTime} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.ZonedDateTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<ZonedDateTime, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain ZonedDateTime#toString() toString method} to convert.</p>
         *
         * @param zonedDateTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(ZonedDateTime zonedDateTime) {
            return ((zonedDateTime == null) ? null : zonedDateTime.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain ZonedDateTime#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.ZonedDateTime) convertToDatabaseColumn
         */
        @Override
        public ZonedDateTime convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
