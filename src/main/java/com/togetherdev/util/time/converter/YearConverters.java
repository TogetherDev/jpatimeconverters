/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.Year;
import static java.time.Year.of;
import static java.time.Year.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain Year}.
 *
 * @author Thomás Sousa Silva
 */
public final class YearConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private YearConverters() {
        throw new AssertionError("No YearConverters instances for you!");
    }

    /**
     * Converter of {@linkplain Year} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.Year) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToInteger implements AttributeConverter<Year, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Year#getValue() getValue method} to convert.</p>
         *
         * @param year The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(Year year) {
            return ((year == null) ? null : year.getValue());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Year#of(int) of method} to convert.</p>
         *
         * @param year The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Year) convertToDatabaseColumn
         */
        @Override
        public Year convertToEntityAttribute(Integer year) {
            return ((year == null) ? null : of(year));
        }

    }

    /**
     * Converter of {@linkplain Year} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.Year) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<Year, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Year#toString() toString method} to convert.</p>
         *
         * @param year The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(Year year) {
            return ((year == null) ? null : year.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Year#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Year) convertToDatabaseColumn
         */
        @Override
        public Year convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
