/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.time.ZoneOffset;
import static java.time.ZoneOffset.of;
import static java.time.ZoneOffset.ofTotalSeconds;
import static java.time.temporal.ChronoField.OFFSET_SECONDS;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain ZoneOffset}.
 *
 * @author Thomás Sousa Silva
 */
public final class ZoneOffsetConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private ZoneOffsetConverters() {
        throw new AssertionError("No ZoneOffsetConverters instances for you!");
    }

    /**
     * Converter of {@linkplain ZoneOffset} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.ZoneOffset) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToSeconds implements AttributeConverter<ZoneOffset, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain ZoneOffset#get(java.time.temporal.TemporalField) get method} to convert, using the {@linkplain java.time.temporal.ChronoField#OFFSET_SECONDS OFFSET_SECONDS}.</p>
         *
         * @param zoneOffset The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(ZoneOffset zoneOffset) {
            return ((zoneOffset == null) ? null : zoneOffset.get(OFFSET_SECONDS));
        }

        @Override
        public ZoneOffset convertToEntityAttribute(Integer seconds) {
            return ((seconds == null) ? null : ofTotalSeconds(seconds));
        }

    }

    /**
     * Converter of {@linkplain ZoneOffset} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.ZoneOffset) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<ZoneOffset, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain ZoneOffset#getId() getId method} to convert.</p>
         *
         * @param zoneOffset The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(ZoneOffset zoneOffset) {
            return ((zoneOffset == null) ? null : zoneOffset.getId());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain ZoneOffset#of(java.lang.String) of method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.ZoneOffset) convertToDatabaseColumn
         */
        @Override
        public ZoneOffset convertToEntityAttribute(String text) {
            return ((text == null) ? null : of(text));
        }

    }

}
