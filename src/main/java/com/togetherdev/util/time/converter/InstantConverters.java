/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.sql.Timestamp;
import static java.sql.Timestamp.from;
import java.time.Instant;
import static java.time.Instant.ofEpochMilli;
import static java.time.Instant.ofEpochSecond;
import static java.time.Instant.parse;
import java.util.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain Instant}.
 *
 * @author Thomás Sousa Silva
 */
public final class InstantConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private InstantConverters() {
        throw new AssertionError("No InstantConverters instances for you!");
    }

    /**
     * Converter of {@linkplain Instant} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToEpochMilli implements AttributeConverter<Instant, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Instant#toEpochMilli() toEpochMilli method} to convert.</p>
         *
         * @param instant The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Instant instant) {
            return ((instant == null) ? null : instant.toEpochMilli());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Instant#ofEpochMilli(long) ofEpochMilli method} to convert.</p>
         *
         * @param epochMilli The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
         */
        @Override
        public Instant convertToEntityAttribute(Long epochMilli) {
            return ((epochMilli == null) ? null : ofEpochMilli(epochMilli));
        }

    }

    /**
     * Converter of {@linkplain Instant} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToEpochSecond implements AttributeConverter<Instant, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Instant#getEpochSecond() getEpochSecond method} to convert.</p>
         *
         * @param instant The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(Instant instant) {
            return ((instant == null) ? null : instant.getEpochSecond());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Instant#ofEpochSecond(long) ofEpochSecond method} to convert.</p>
         *
         * @param epochSecond The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
         */
        @Override
        public Instant convertToEntityAttribute(Long epochSecond) {
            return ((epochSecond == null) ? null : ofEpochSecond(epochSecond));
        }

    }

    /**
     * Converter of {@linkplain Instant} to {@linkplain Date}.
     *
     * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.util.Date) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToDate implements AttributeConverter<Instant, Date> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Date#from(java.time.Instant) from method} to convert.</p>
         *
         * @param instant The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.util.Date) convertToEntityAttribute
         */
        @Override
        public Date convertToDatabaseColumn(Instant instant) {
            return ((instant == null) ? null : from(instant));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Date#toInstant() toInstant method} to convert.</p>
         *
         * @param date The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
         */
        @Override
        public Instant convertToEntityAttribute(Date date) {
            return ((date == null) ? null : date.toInstant());
        }

    }

    /**
     * Converter of {@linkplain Instant} to {@linkplain Timestamp}.
     *
     * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.sql.Timestamp) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToTimestamp implements AttributeConverter<Instant, Timestamp> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Timestamp#from(java.time.Instant) from method} to convert.</p>
         *
         * @param instant The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.sql.Timestamp) convertToEntityAttribute
         */
        @Override
        public Timestamp convertToDatabaseColumn(Instant instant) {
            return ((instant == null) ? null : from(instant));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Timestamp#toInstant() toInstant method} to convert.</p>
         *
         * @param timestamp The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
         */
        @Override
        public Instant convertToEntityAttribute(Timestamp timestamp) {
            return ((timestamp == null) ? null : timestamp.toInstant());
        }

    }

    /**
     * Converter of {@linkplain Instant} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<Instant, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Instant#toString() toString method} to convert.</p>
         *
         * @param instant The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(Instant instant) {
            return ((instant == null) ? null : instant.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Instant#parse(java.lang.CharSequence) toInstant method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.Instant) convertToDatabaseColumn
         */
        @Override
        public Instant convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
