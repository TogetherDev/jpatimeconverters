/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.time.converter;

import java.sql.Time;
import static java.sql.Time.valueOf;
import java.time.LocalTime;
import static java.time.LocalTime.ofNanoOfDay;
import static java.time.LocalTime.ofSecondOfDay;
import static java.time.LocalTime.parse;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * This class groups converters of {@linkplain LocalTime}.
 *
 * @author Thomás Sousa Silva
 */
public final class LocalTimeConverters {

    /**
     * @throws AssertionError All times that it is called.
     */
    private LocalTimeConverters() {
        throw new AssertionError("No LocalTimeConverters instances for you!");
    }

    /**
     * Converter of {@linkplain LocalTime} to {@linkplain Time}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.sql.Time) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToTime implements AttributeConverter<LocalTime, Time> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain Time#valueOf(java.time.LocalTime) valueOf method} to convert.</p>
         *
         * @param localTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.sql.Time) convertToEntityAttribute
         */
        @Override
        public Time convertToDatabaseColumn(LocalTime localTime) {
            return ((localTime == null) ? null : valueOf(localTime));
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain Time#toLocalTime() toLocalTime method} to convert.</p>
         *
         * @param time The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
         */
        @Override
        public LocalTime convertToEntityAttribute(Time time) {
            return ((time == null) ? null : time.toLocalTime());
        }

    }

    /**
     * Converter of {@linkplain LocalTime} to {@linkplain Integer}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToSecondOfDay implements AttributeConverter<LocalTime, Integer> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#toSecondOfDay() toSecondOfDay method} to convert.</p>
         *
         * @param localTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Integer) convertToEntityAttribute
         */
        @Override
        public Integer convertToDatabaseColumn(LocalTime localTime) {
            return ((localTime == null) ? null : localTime.toSecondOfDay());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#ofSecondOfDay(long) ofSecondOfDay method} to convert.</p>
         *
         * @param secondOfDay The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
         */
        @Override
        public LocalTime convertToEntityAttribute(Integer secondOfDay) {
            return ((secondOfDay == null) ? null : ofSecondOfDay(secondOfDay));
        }

    }

    /**
     * Converter of {@linkplain LocalTime} to {@linkplain Long}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToNanoOfDay implements AttributeConverter<LocalTime, Long> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#toNanoOfDay() toNanoOfDay method} to convert.</p>
         *
         * @param localTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.Long) convertToEntityAttribute
         */
        @Override
        public Long convertToDatabaseColumn(LocalTime localTime) {
            return ((localTime == null) ? null : localTime.toNanoOfDay());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#ofNanoOfDay(long) ofNanoOfDay method} to convert.</p>
         *
         * @param nanoOfDay The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
         */
        @Override
        public LocalTime convertToEntityAttribute(Long nanoOfDay) {
            return ((nanoOfDay == null) ? null : ofNanoOfDay(nanoOfDay));
        }

    }

    /**
     * Converter of {@linkplain LocalTime} to {@linkplain String}.
     *
     * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
     * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
     * @author Thomás Sousa Silva
     */
    @Converter
    public static class ToString implements AttributeConverter<LocalTime, String> {

        /**
         * <p>
         * Converts the given value to a value that can be stored in a database.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#toString() toString method} to convert.</p>
         *
         * @param localTime The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToEntityAttribute(java.lang.String) convertToEntityAttribute
         */
        @Override
        public String convertToDatabaseColumn(LocalTime localTime) {
            return ((localTime == null) ? null : localTime.toString());
        }

        /**
         * <p>
         * Converts the given value to a value that can be stored in a entity.</p>
         * <p>
         * This method uses the {@linkplain LocalTime#parse(java.lang.CharSequence) parse method} to convert.</p>
         *
         * @param text The value that will be converted.
         * @return The conversion result or null if the given value is null.
         * @see #convertToDatabaseColumn(java.time.LocalTime) convertToDatabaseColumn
         */
        @Override
        public LocalTime convertToEntityAttribute(String text) {
            return ((text == null) ? null : parse(text));
        }

    }

}
